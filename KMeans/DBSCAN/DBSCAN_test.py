import numpy as np
import pandas as pd
from pandas import plotting
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns

from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler
from sklearn import metrics
from sklearn.metrics import pairwise_distances

# import some data to play with
df = pd.read_csv('../stage3_code.csv',
                 dtype={'life_satisfaction': np.float64, 'worthwhile': np.float64, 'happiness': np.float64,
                        'anxiety': np.float64},
                 index_col=0,
                 names=['life_satisfaction', 'worthwhile', 'happiness', 'anxiety'], header=0)

# generate
X = df.iloc[:, :3]
X = StandardScaler().fit_transform(X)

db = DBSCAN(eps=0.2, min_samples=5).fit(X)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_

# Number of clusters in labels, ignoring noise if present.
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
n_noise_ = list(labels).count(-1)

print('Estimated number of clusters: %d' % n_clusters_)
print('Estimated number of noise points: %d' % n_noise_)

color = ['r', 'g', 'c', 'y', 'm', 'b', 'pink', 'maroon', 'tomato', 'peru', 'lawngreen', 'gold', 'aqua', 'dodgerblue']
ax = plt.subplot(111, projection='3d')
for i in range(0, n_clusters_):
    pos = n_clusters_[i]
    if i == "noisy":
        for p in pos:
            ax.scatter(X[p, 0], X[p, 1], X[p, 2], c='black')
    else:
        for p in pos:
            ax.scatter(X[p, 0], X[p, 1], X[p, 2], c=color[i])
plt.show()
