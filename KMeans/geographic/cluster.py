import numpy as np
import pandas as pd
from pandas import plotting
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn import metrics
from sklearn.metrics import pairwise_distances

df = pd.read_csv('../code_geo.csv',
                 dtype={'life_satisfaction': np.float64, 'worthwhile': np.float64, 'happiness': np.float64,
                        'anxiety': np.float64, 'LONG': np.float64, 'LAT': np.float64},
                 names=['area_code', 'LONG', 'LAT', 'life_satisfaction', 'worthwhile', 'happiness', 'anxiety'],
                 header=0)
df.dropna(axis=0, how='any', inplace=True)
# print(df)

# df = df[['LONG', 'LAT']]

# df = df[df['anxiety'] > 3]
# print(X)
X_geo = df[['LONG', 'LAT', 'anxiety']]
# X = StandardScaler().fit_transform(X)
# print(X_geo)
# X_geo['LONG'] = X_geo['LONG']
# X_geo['LAT'] = X_geo['LAT']

''''
x = np.array(X_geo['LAT'])
y = np.array(X_geo['LONG'])
fig = plt.figure()
plt.scatter(x, y, alpha=0.5)
plt.show()
'''

kmeans = KMeans(n_clusters=3).fit(X_geo)
# predict = kmeans.predict(X_geo)
X_geo['cluster'] = kmeans.labels_
# X_geo['cluster_centers'] = kmeans.cluster_centers_
# print(X_geo['cluster'])

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
x = np.array(X_geo['LAT'])
y = np.array(X_geo['LONG'])
z = np.array(X_geo['anxiety'])

'''
colors = ['b', 'g', 'r']
# print color
for i, c in enumerate(X_geo['cluster_n'][:]):
    plt.scatter(x[i], y[i], c=colors[c], alpha=0.5)
plt.show()
'''
# print(X_geo['cluster'])
for i in range(3):
    # v = []
    # for j in range(df.shape[0]):
    #     if X_geo.loc[j, 'cluster'] == i:
    #         v.append(X_geo.loc[j, 'anxiety'])
    # avg = np.mean(v)
    cent = kmeans.cluster_centers_[i]
    print(i, cent)

'''
ax.scatter(x, y, z, c=kmeans.labels_)
plt.title('KMeans n=3')
plt.show()
'''
plt.scatter(X_geo['LAT'], X_geo['LONG'], c=kmeans.labels_)
plt.show()

'''
db = DBSCAN(eps=0.3, min_samples=10).fit(X)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_

# Number of clusters in labels, ignoring noise if present.
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
n_noise_ = list(labels).count(-1)
print('Estimated number of clusters: %d' % n_clusters_)
print('Estimated number of noise points: %d' % n_noise_)
print("Silhouette Coefficient: %0.3f" % metrics.silhouette_score(X, labels))

unique_labels = set(labels)
colors = [plt.cm.Spectral(each)
          for each in np.linspace(0, 1, len(unique_labels))]
for k, col in zip(unique_labels, colors):
    if k == -1:
        # Black used for noise.
        col = [0, 0, 0, 1]

    class_member_mask = (labels == k)

    xy = X[class_member_mask & core_samples_mask]
    plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k', markersize=14)

    xy = X[class_member_mask & ~core_samples_mask]
    plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k', markersize=6)

plt.title('Estimated number of clusters: %d' % n_clusters_)
plt.show()
'''
