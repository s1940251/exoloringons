import numpy as np
import pandas as pd
from pandas import plotting
import matplotlib.pyplot as plt

from sklearn.cluster import DBSCAN
from sklearn import metrics

import descartes
import geopandas as gpd
from shapely.geometry import Point, Polygon

df = pd.read_csv('../code_geo.csv',
                 dtype={'life_satisfaction': np.float64, 'worthwhile': np.float64, 'happiness': np.float64,
                        'anxiety': np.float64, 'LONG': np.float64, 'LAT': np.float64},
                 names=['area_code', 'LONG', 'LAT', 'life_satisfaction', 'worthwhile', 'happiness', 'anxiety'],
                 header=0)
df.dropna(axis=0, how='any', inplace=True)
# print(df)

X = df.values[:, 3:]
db = DBSCAN(eps=0.2, min_samples=5).fit(X)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_
df['cluster'] = labels
# plt.scatter(df.values[:, 1], df.values[:, 2], c=labels)
# plt.show()
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
n_noise_ = list(labels).count(-1)

print('Estimated number of clusters: %d' % n_clusters_)
print('Estimated number of noise points: %d' % n_noise_)
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(X, labels))

uk_map = gpd.read_file(
    '/Users/gilbert/PycharmProjects/Dissertation/KMeans/geographic'
    '/Local_Authority_Districts__December_2019__Boundaries_UK_BFE-shp'
    '/Local_Authority_Districts__December_2019__Boundaries_UK_BFE.shp')
uk_map = uk_map.to_crs(epsg=4326)
fig, ax = plt.subplots(figsize=(15, 15))
uk_map.plot(ax=ax, color="grey")
# plt.show()

geometry = [Point(xy) for xy in zip(df["LONG"], df["LAT"])]
# print(geometry[:3])
crs = {'init': 'epsg:4326'}
geo_df = gpd.GeoDataFrame(df,
                          crs=crs,
                          geometry=geometry)
# print(geo_df.head())
'''
geo_df[geo_df['cluster'] == 0].plot(ax=ax, markersize=10, color="green", label="0")
geo_df[geo_df['cluster'] == 1].plot(ax=ax, markersize=10, color="red", label="1")
geo_df[geo_df['cluster'] == 2].plot(ax=ax, markersize=10, color="blue", label="2")
plt.legend(prop={'size': 15})

plt.show()
'''