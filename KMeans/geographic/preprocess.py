import pandas as pd
import numpy as np
import os
import csv
from dbfread import DBF

csv_file = "/Users/gilbert/PycharmProjects/Dissertation/Local_Authority_Districts_(April_2019)_Boundaries_UK_BFE.csv"
df1 = pd.read_csv(csv_file, header=0)
df1.head()
# print(df)

# df1 = df1[['FID', 'LAD19CD', 'LAD19NMW', 'BNG_E', 'BNG_N', 'LONG', 'LAT', 'Shape__Area', 'Shape__Length']]
# df1.head()
df1.rename(columns={'LAD19CD': 'area_code'}, inplace=True)
# print(df1)

df2 = pd.read_csv('../stage3_code.csv',
                  dtype={'life_satisfaction': np.float64, 'worthwhile': np.float64, 'happiness': np.float64,
                         'anxiety': np.float64},
                  names=['area_code', 'life_satisfaction', 'worthwhile', 'happiness', 'anxiety'], header=0)
# print(df2)

df = pd.merge(df1, df2, how='right', on='area_code')
df.drop(df.columns[[0, 2, 3, 4, 5, 8, 9]], axis=1, inplace=True)
print(df)

output_file = "./code_geo.csv"
df.to_csv(output_file, sep=',', index=False, header=True)
