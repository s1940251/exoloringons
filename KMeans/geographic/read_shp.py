import numpy as np
import pandas as pd
from pandas import plotting
# from PIL import Image
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

from sklearn.cluster import KMeans

import descartes
import geopandas as gpd
from shapely.geometry import Point, Polygon

import shapefile

shps = shapefile.Reader('/Users/gilbert/PycharmProjects/Dissertation/KMeans/geographic'
                        '/Local_Authority_Districts__December_2019__Boundaries_UK_BFE-shp'
                        '/Local_Authority_Districts__December_2019__Boundaries_UK_BFE.shp')
# shps.plot()
# plt.show()
shp = shps.shapeRecord()
# print(shps.crs)
# print(shp.shape.points)

# img1 = mpimg.imread("/Users/gilbert/PycharmProjects/Dissertation/KMeans/geographic/uk_district.png")
img2 = mpimg.imread("/Users/gilbert/PycharmProjects/Dissertation/KMeans/geographic/cluster_kmeans.png")
img3 = mpimg.imread("/Users/gilbert/PycharmProjects/Dissertation/KMeans/geographic/cluster_gaussian.png")
img4 = mpimg.imread("/Users/gilbert/PycharmProjects/Dissertation/KMeans/geographic/cluster_agglomerative.png")

plt.figure()
# plt.subplot(2,2,1)
# plt.imshow(img1)
plt.subplot(1,3,1)
plt.imshow(img2)
plt.subplot(1,3,2)
plt.imshow(img3)
plt.subplot(1,3,3)
plt.imshow(img4)
plt.show()
