import numpy as np
import pandas as pd
from pandas import plotting
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns

from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn import metrics
from sklearn.metrics import pairwise_distances

# import some data to play with
df = pd.read_csv('../stage3_code.csv',
                 dtype={'life_satisfaction': np.float64, 'worthwhile': np.float64, 'happiness': np.float64,
                        'anxiety': np.float64},
                 index_col=0,
                 names=['life_satisfaction', 'worthwhile', 'happiness', 'anxiety'], header=0)

# print(df)
X = df.iloc[:, :3]
# print(X)
kmeans = KMeans(n_clusters=3, random_state=0)
kmeans.fit(X)
labels = kmeans.labels_
# print(metrics.silhouette_score(df, labels, metric='euclidean'))

predict = kmeans.predict(X)
X['cluster'] = predict
# plotting.parallel_coordinates(X, 'cluster')
# plt.show()

# plt.figure()
# ax1 = plt.subplot(1, 2, 1)
# ax2 = plt.subplot(1, 2, 2)

X_1 = pd.DataFrame(X)
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(X_1.iloc[:, 0].values, X_1.iloc[:, 1].values, X_1.iloc[:, 2].values,
           c=predict)
# plt.show()

# anx = df.values[:, 3]
# plt.scatter(anx)
df['label'] = labels

# print(df)
# sns.boxplot(x='label', y='anxiety', data=df.iloc[:, 3:])
data_1 = df.iloc[:, 3:]
# print(data_1)
# data_1.plot(kind="box", subplots=True, layout=(3,3), sharex=False, sharey=False)
sns.FacetGrid(data_1, hue='label', size=6).map(sns.kdeplot, 'anxiety').add_legend()
print(metrics.silhouette_score(df, labels, metric='euclidean'))
plt.show()
