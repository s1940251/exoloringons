import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline

# import some data to play with
df = pd.read_csv('../stage3_code.csv',
                 dtype={'life_satisfaction': np.float64, 'worthwhile': np.float64, 'happiness': np.float64,
                        'anxiety': np.float64},
                 index_col=0,
                 names=['life_satisfaction', 'worthwhile', 'happiness', 'anxiety'], header=0)
# scaler = StandardScaler()

wx = []
for i in range(1, 11):
    kmeans = KMeans(n_clusters=i, random_state=0)
    # pipeline = make_pipeline(scaler, kmeans)
    kmeans.fit(df)
    # pipeline.fit(df)
    wx.append(kmeans.inertia_)
    # wx.append(pipeline.inertia_)
plt.plot(range(1, 11), wx)
plt.xlabel('Number of clusters')
plt.ylabel('Variance Explained')
plt.show()
