import numpy as np
import pandas as pd
from pandas import plotting
import matplotlib.pyplot as plt


from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn import metrics
from sklearn.metrics import pairwise_distances

# import some data to play with
df = pd.read_csv('../stage3_code.csv',
                 dtype={'life_satisfaction': np.float64, 'worthwhile': np.float64, 'happiness': np.float64,
                        'anxiety': np.float64},
                 index_col=0,
                 names=['life_satisfaction', 'worthwhile', 'happiness', 'anxiety'], header=0)

kmeans = KMeans(n_clusters=3, random_state=0)
kmeans.fit(df)
labels = kmeans.labels_
print(metrics.silhouette_score(df, labels, metric='euclidean'))


'''
predict = kmeans.predict(df)
df['cluster'] = predict
plotting.parallel_coordinates(df, 'cluster')
plt.show()
'''



