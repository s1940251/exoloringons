import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from pandas.plotting import scatter_matrix

df = pd.read_csv('../stage3_code.csv',
                 dtype={'life_satisfaction': np.float64, 'worthwhile': np.float64, 'happiness': np.float64,
                        'anxiety': np.float64},
                 index_col=0,
                 names=['life_satisfaction', 'worthwhile', 'happiness', 'anxiety'], header=0)

# df.hist()
scatter_matrix(df)
plt.show()