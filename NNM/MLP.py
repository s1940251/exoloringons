import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sklearn
from sklearn.neural_network import MLPClassifier
from sklearn.neural_network import MLPRegressor

from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from math import sqrt
from sklearn.metrics import r2_score

df = pd.read_csv('../stage3_code.csv',
                 dtype={'life_satisfaction': np.float64, 'worthwhile': np.float64, 'happiness': np.float64,
                        'anxiety': np.float64},
                 index_col=0,
                 names=['life_satisfaction', 'worthwhile', 'happiness', 'anxiety'], header=0)

y0 = df['anxiety'].values
y1 = []
for i in y0:
    if 2.48 < i < 3.14:
        j = 1
    elif i <= 2.48:
        j = 0
    else:
        j = 2
    y1.append(j)
df['anxiety_'] = y1
# print(df)
# print(df.shape)
# print(df.describe().transpose())

X = df.drop('anxiety', axis=1).drop('anxiety_', axis=1)
y = df['anxiety_']
print(y)

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y)

from sklearn.preprocessing import StandardScaler

scaler = StandardScaler()
scaler.fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)

from sklearn.neural_network import MLPClassifier

mlp = MLPClassifier()
mlp.fit(X_train, y_train)
predictions = mlp.predict(X_test)

from sklearn.metrics import classification_report, confusion_matrix

print(confusion_matrix(y_test, predictions))
print(classification_report(y_test, predictions))
