import warnings

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import MinMaxScaler
from sklearn import datasets
from sklearn.exceptions import ConvergenceWarning
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import accuracy_score

# different learning rate schedules and momentum parameters
params = [{'hidden_layer_sizes': (200,)},
          {'hidden_layer_sizes': (100,)},
          {'hidden_layer_sizes': (50,)},
          {'hidden_layer_sizes': (25,)},
          {'hidden_layer_sizes': (10,)},
          {'hidden_layer_sizes': (5,)},
          {'hidden_layer_sizes': (1,)}]

labels = ["(200,)", "(100,)", "(50,)", "(25,)", "(10,)", "(5,)", "(1,)"]

plot_args = [{'c': 'red', 'linestyle': '-'},
             {'c': 'green', 'linestyle': '-'},
             {'c': 'blue', 'linestyle': '-'},
             {'c': 'red', 'linestyle': '--'},
             {'c': 'green', 'linestyle': '--'},
             {'c': 'blue', 'linestyle': '--'},
             {'c': 'black', 'linestyle': '-'}]


def plot_on_dataset(X_train, y_train, X_test, y_test, ax, name):
    # for each dataset, plot learning for each learning strategy
    print("\nlearning on dataset %s" % name)
    ax.set_title(name)

    X_train = MinMaxScaler().fit_transform(X_train)
    mlps = []
    if name == "digits":
        # digits is larger but converges fairly quickly
        max_iter = 15
    else:
        max_iter = 400

    for label, param in zip(labels, params):
        print("training: %s" % label, file=f)
        mlp = MLPClassifier(random_state=0,
                            max_iter=max_iter, **param)

        # some parameter combinations will not converge as can be seen on the
        # plots so they are ignored here
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", category=ConvergenceWarning,
                                    module="sklearn")
            mlp.fit(X_train, y_train)
            predictions = mlp.predict(X_test)

        mlps.append(mlp)
        print("Training set score: %f" % mlp.score(X_train, y_train), file=f)
        print("Training set loss: %f" % mlp.loss_, file=f)
        # print(classification_report(y_test, predictions))
        print("accuraccy: %f\n" % accuracy_score(y_test, predictions), file=f)
    for mlp, label, args in zip(mlps, labels, plot_args):
        ax.plot(mlp.loss_curve_, label=label, **args)


fig, axes = plt.subplots(2, 2, figsize=(15, 10))
# load / generate some toy datasets
'''
iris = datasets.load_iris()
X_digits, y_digits = datasets.load_digits(return_X_y=True)
data_sets = [(iris.data, iris.target),
             (X_digits, y_digits),
             datasets.make_circles(noise=0.2, factor=0.5, random_state=1),
             datasets.make_moons(noise=0.3, random_state=0)]

for ax, data, name in zip(axes.ravel(), data_sets, ['iris', 'digits',
                                                    'circles', 'moons']):
    plot_on_dataset(*data, ax=ax, name=name)

fig.legend(ax.get_lines(), labels, ncol=3, loc="upper center")
plt.show()
'''

df = pd.read_csv('../stage3_code.csv',
                 dtype={'life_satisfaction': np.float64, 'worthwhile': np.float64, 'happiness': np.float64,
                        'anxiety': np.float64},
                 index_col=0,
                 names=['life_satisfaction', 'worthwhile', 'happiness', 'anxiety'], header=0)

y0 = df['anxiety'].values
y1 = []
for i in y0:
    if 2.48 < i < 3.14:
        j = 1
    elif i <= 2.48:
        j = 0
    else:
        j = 2
    y1.append(j)
df['anxiety_'] = y1
# print(df)
# print(df.shape)
# print(df.describe().transpose())

X = df.drop('anxiety', axis=1).drop('anxiety_', axis=1)
y = df['anxiety_']
X_train, X_test, y_train, y_test = train_test_split(X, y)

f = open(r'./neurons.txt', 'w')
for ax in axes.ravel():
    X_train, X_test, y_train, y_test = train_test_split(X, y)
    plot_on_dataset(X_train, y_train, X_test, y_test, ax=ax, name='anxiety')
    print("=========================\n", file=f)

fig.legend(ax.get_lines(), labels, ncol=3, loc="upper center")
plt.show()
