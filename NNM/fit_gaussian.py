import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.stats import norm
import math


def func(x, a, u, sig):
    return a * np.exp(-(x - u) ** 2 / (2 * sig ** 2)) / (sig * math.sqrt(2 * math.pi))


df = pd.read_csv('../stage3_code.csv',
                 dtype={'life_satisfaction': np.float64, 'worthwhile': np.float64, 'happiness': np.float64,
                        'anxiety': np.float64},
                 index_col=0,
                 names=['life_satisfaction', 'worthwhile', 'happiness', 'anxiety'], header=0)

anx = df['anxiety'].values
# print(anx)
x = np.array(anx)
mu = np.mean(x)
sigma = np.std(x)
num_bins = 30
n, bins, patches = plt.hist(x, num_bins, density=1, alpha=0.75)

y = norm.pdf(bins, mu, sigma)

plt.grid(True)
plt.plot(bins, y, 'r--')
plt.xlabel('values')
plt.ylabel('Probability')
plt.title('Histogram : $\mu$=' + str(round(mu, 2)) + ' $\sigma=$' + str(round(sigma, 2)))
plt.show()
