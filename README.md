# Exploring ONS Well-Being Datasets with Machine Learning

## Project information

**Student name: Jiaqi Liu** 

**Student number: s1940251** 

**EPCC supervisor name(s): Adam Carter** 

<!--
## Description
-->

<!--
This repository serves as an example of MSc project repository.
The repository should clearly identify the student and project, 
and contain all relevant information, in a reasonably structured way. 
Note that it is meant to serve as an example and you do not have to adhere to 
the presented structure, you are free to structure your repository in any way 
suitable for you, your project and your supervisor. 
Your repository (relevance of content, it's structure, frequency and quality of 
updates etc.) is an assessed part of the Project Preparation course and 
contributes 10% towards the final mark. Further details about marking the 
repository were given during Lecture 2, and are available on Learn.
-->

## Content

<!--
The repository should contain the following:
-->
* The original dataset

geographicbreakdownreferencetable_tcm77-417203.xls

* Results of preprocessing

/prepocess

* Results of KMeans and other clustering algorithms

/KMeans

* Results of K nearest neighbor

/KNN

* Results of Neural network model

/NNM
