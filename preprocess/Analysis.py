import pandas as pd
import os
import csv
from dbfread import DBF


# 读csv
def read_csv(filename):
    inverse_dict = {}
    with open(filename, 'r') as f:
        r = csv.reader(f)
        for row in r:
            print(row)


# 读dbf
def read_dbf(filename):
    table = DBF(filename)
    for field in table.fields:
        print(field)
    #for record in table:
        # print(record)
        # for field in record:
            # print(record['lad17cd'], end=",")
        # print()


# 拼接


# 写新csv


if __name__ == "__main__":
    csv_file = "stage3_code.csv"
    # read_csv(csv_file)
    dbf_file = "Local_Authority_Districts_December_2017_Full_Clipped_Boundaries_in_United_Kingdom_WGS84.dbf"
    read_dbf(dbf_file)
