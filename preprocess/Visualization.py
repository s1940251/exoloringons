import xlrd
import requests
from bs4 import BeautifulSoup
import bs4
import csv

"""
# 把excel中数据读入python，并存为字典形式
# 输出1: dict = {area_code: rating, ...}
# 输出2：dict = {location: rating, ...}
def read_file(filename):
    workbook = xlrd.open_workbook(filename)
    sheet_names = workbook.sheet_names()
    print("sheet_names are:", sheet_names)
    # 第一个sheet是contents所以不读
    for i in range(1, 5):
        sheet = workbook.sheets()[i]
        sheet_name = workbook.sheet_names()[i]
        locals()[sheet_name] = {}
        # print(sheet_name)
        # rows = sheet.nrows
        # print(rows)
        for row in range(6, 448):
            # 输出1的key
            # area_code = sheet.cell(row, 0).value
            # 输出2的key
            location = sheet.cell(row, 1).value + sheet.cell(row, 2).value + sheet.cell(row, 3).value
            rating = sheet.cell(row, 8).value
            # locals()[sheet_name][area_code] = rating
            locals()[sheet_name][location] = rating
        # print(sheet_name, "=", locals()[sheet_name]['Ythan'])
"""


# 把excel中数据读入python，并存为字典形式
# 输出: stage1 = {area_name: [l, w, h, a], ...}
def read_file2(filename, stage1, stage2, stage3):
    workbook = xlrd.open_workbook(filename)
    sheet_names = workbook.sheet_names()
    print("sheet_names are:", sheet_names)
    # 第一个sheet是contents所以不读
    for i in range(1, 5):
        sheet = workbook.sheets()[i]
        # sheet_name = workbook.sheet_names()[i]
        # locals()[sheet_name] = {}
        for row in range(6, 448):
            rating = sheet.cell(row, 8).value
            # print(rating)
            if sheet.cell(row, 1).value:
                area_name = sheet.cell(row, 1).value.strip()
                # print(area_name)
                if area_name in stage1:
                    # stage1[area_name][i-1] = rating
                    stage1[area_name].append(rating)
                else:
                    stage1[area_name] = []
                    stage1[area_name].append(rating)
            elif sheet.cell(row, 2).value:
                area_name = sheet.cell(row, 2).value.strip()
                if area_name in stage2:
                    # stage2[area_name][i - 1] = rating
                    stage2[area_name].append(rating)
                else:
                    stage2[area_name] = []
                    stage2[area_name].append(rating)
            else:
                area_name = sheet.cell(row, 3).value.strip()
                if area_name in stage3:
                    # stage3[area_name][i - 1] = rating
                    stage3[area_name].append(rating)
                else:
                    stage3[area_name] = []
                    stage3[area_name].append(rating)


# 把excel中数据读入python，并存为字典形式
# 输出: stage1 = {area_code: [l, w, h, a], ...}
def read_file3(filename, stage1, stage2, stage3):
    workbook = xlrd.open_workbook(filename)
    sheet_names = workbook.sheet_names()
    print("sheet_names are:", sheet_names)
    # 第一个sheet是contents所以不读
    for i in range(1, 5):
        sheet = workbook.sheets()[i]
        # sheet_name = workbook.sheet_names()[i]
        # locals()[sheet_name] = {}
        for row in range(6, 448):
            rating = sheet.cell(row, 8).value
            # print(rating)
            if sheet.cell(row, 1).value:
                # area_name = sheet.cell(row, 1).value.strip()
                area_code = sheet.cell(row, 0).value
                # print(area_name)
                if area_code in stage1:
                    # stage1[area_name][i-1] = rating
                    stage1[area_code].append(rating)
                else:
                    stage1[area_code] = []
                    stage1[area_code].append(rating)
            elif sheet.cell(row, 2).value:
                # area_name = sheet.cell(row, 2).value.strip()
                area_code = sheet.cell(row, 0).value
                if area_code in stage2:
                    # stage2[area_name][i - 1] = rating
                    stage2[area_code].append(rating)
                else:
                    stage2[area_code] = []
                    stage2[area_code].append(rating)
            elif sheet.cell(row, 3).value:
                # area_name = sheet.cell(row, 3).value.strip()
                area_code = sheet.cell(row, 0).value
                if area_code in stage3:
                    # stage3[area_name][i - 1] = rating
                    stage3[area_code].append(rating)
                else:
                    stage3[area_code] = []
                    stage3[area_code].append(rating)

"""
# 读网址里的经纬度表格，存为字典
# 所需函数：chech_url, read_url, save_url
# 输出: dict = {location: [latitude, longitude], ...}
def check_url(url):
    try:
        r = requests.get(url)
        r.raise_for_status()
        r.encoding = r.apparent_encoding
        return r.text
    except:
        print('ERROR: cannot connect')


def read_url(rurl, url_list):
    soup = BeautifulSoup(rurl, 'html.parser')
    trs = soup.find_all('tr')
    for tr in trs:
        ui = []
        for td in tr:
            # 不知为何会有空字符串，过滤一下
            if td.string != ' ':
                ui.append(td.string)
        url_list.append(ui)


def save_url(url_list):
    inverse_dict = {}
    for i in range(5, len(url_list)):
        location = url_list[i][0].strip()
        latitude = url_list[i][1]
        longitude = url_list[i][2]
        inverse_dict[location] = [latitude, longitude]
    return inverse_dict
"""


# 字典转为list的函数，方便存入csv
def dict_to_list(stage):
    inverse_list = []
    for area_name, wb_list in stage.items():
        each_list = wb_list
        each_list.insert(0, area_name)
        inverse_list.append(each_list)
    return inverse_list


# 把三个stage字典存入csv方便使用
def write_csv(stage1, stage2, stage3):
    with open('stage1_code.csv', 'w') as f:
        # fieldnames = ['area_name', 'life_satisfaction', 'worthwhile', 'happiness', 'anxiety']
        w = csv.writer(f)
        w.writerow(['area_code', 'life_satisfaction', 'worthwhile', 'happiness', 'anxiety'])
        w.writerows(dict_to_list(stage1))
    with open('stage2_code.csv', 'w') as f:
        # fieldnames = ['area_name', 'life_satisfaction', 'worthwhile', 'happiness', 'anxiety']
        w = csv.writer(f)
        w.writerow(['area_code', 'life_satisfaction', 'worthwhile', 'happiness', 'anxiety'])
        w.writerows(dict_to_list(stage2))
    with open('stage3_code.csv', 'w') as f:
        # fieldnames = ['area_name', 'life_satisfaction', 'worthwhile', 'happiness', 'anxiety']
        w = csv.writer(f)
        w.writerow(['area_code', 'life_satisfaction', 'worthwhile', 'happiness', 'anxiety'])
        w.writerows(dict_to_list(stage3))


if __name__ == "__main__":
    file_geo = "/Users/gilbert/PycharmProjects/Dissertation/geographicbreakdownreferencetable_tcm77-417203.xls"
    stage1 = {}
    stage2 = {}
    stage3 = {}
    # read_file2(file_geo, stage1, stage2, stage3)
    stage1_code = {}
    stage2_code = {}
    stage3_code = {}
    read_file3(file_geo, stage1_code, stage2_code, stage3_code)
    # write_csv(stage1, stage2, stage3)
    write_csv(stage1_code, stage2_code, stage3_code)
    # print(stage1)
    # url不顶用啊
    '''
    url = "https://www.mapsofworld.com/lat_long/united-kingdom-lat-long.html"
    url_list = []
    rurl = check_url(url)
    read_url(rurl, url_list)
    # url_list = filter(None, url_list)
    # print(url_list)
    locations = save_url(url_list)
    print(locations['Ythan'])
    '''