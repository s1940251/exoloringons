import descartes
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
# from pandas.tests.frame.test_validate import dataframe

fp = "Local_Authority_Districts_December_2017_Full_Clipped_Boundaries_in_United_Kingdom_WGS84.shp"

map_df = gpd.read_file(fp)

map_df.head()

map_df.plot()

# plt.show()

df = pd.read_csv("stage2_code.csv", header=0)

df.head()

df = df[['area_code', 'life_satisfaction', 'worthwhile', 'happiness', 'anxiety']]

df.head()

# 地图和数据结合
merged = map_df.set_index('lad17cd').join(df.set_index('area_code'))

merged.head()

# 开始整活
variable = 'anxiety'

vmin, vmax = 2, 4

fig, ax = plt.subplots(1, figsize=(10, 6))

merged.plot(column=variable, cmap='Purples', linewidth=0.8, ax=ax, edgecolor='0.8')

ax.axis('off')

plt.show()

fig.savefig("map_stage2_ax.png", dpi=300)

